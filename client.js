const net = require('net');
const readline = require('readline');
const client = new net.Socket();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.on('line', (line) => {
    sendMessage(line);
    rl.prompt();
});

let username = "default";

client.connect(3000, '127.0.0.1', () => {
    console.log('Conectado al servidor ');
    rl.question("Elige un nombre de usuario: ", (user) => {
        username = user;
        rl.prompt();
    });
});


function sendMessage(message) {
    let data = {
        user: username,
        message: message
    }
    client.write(JSON.stringify(data));
}

client.on('data', (data) => {
    let decoded = JSON.parse(data);
    console.log(`\n\t${decoded.user}: ${decoded.message}`);
}).on('close', () => {
    console.log('Se ha cerrado la conexión.');
    rl.close();
});

