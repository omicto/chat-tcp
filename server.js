const net = require('net');
const connectedSockets = new Set();

// Send this to everyone except one
connectedSockets.broadcast = (data, except) => {
    for (let sock of connectedSockets) {
        if (sock !== except) {
            sock.write(data);
        }
    }
}

const server = net.createServer((connection) => {
    console.log('Someone has connected');
    connectedSockets.add(connection);

    connection.on('end', () => {
        console.log('Someone has left');
        connectedSockets.delete(connection);
    });

    connection.on('data', (data) => {
        try{
            let decoded = JSON.parse(data);
            console.log(`${decoded.user} said ${decoded.message}`);
            connectedSockets.broadcast(data, connection);
        }catch(error){
            connection.write("Hi");
        }
    });
    connection.pipe
});

server.listen(3000, '127.0.0.1');